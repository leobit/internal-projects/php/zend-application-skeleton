<?php

declare(strict_types=1);

namespace app;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221211142944 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $table = $schema->createTable('news');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('title', 'string', ['notnull'=>true, 'length'=>256]);
        $table->addColumn('description', 'string', ['notnull'=>true, 'length'=>512]);
        $table->addColumn('authors', 'string', ['notnull'=>true, 'length'=>256]);
        $table->addColumn('content', 'string', ['notnull'=>true, 'length'=>2048]);
        $table->addColumn('date_created', 'datetime', ['notnull'=>true]);
        $table->addColumn('date_modified', 'datetime', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['title'], 'title_idx');
        $table->addOption('engine' , 'InnoDB');

//        $this->addSql('CREATE TABLE `zend-application-skeleton`.`news` (
//                                `id` INT NOT NULL AUTO_INCREMENT ,
//                                `title` VARCHAR NOT NULL ,
//                                `description` VARCHAR NOT NULL ,
//                                `authors` VARCHAR NOT NULL ,
//                                `content` TEXT NOT NULL ,
//                                `data_created` DATETIME NOT NULL ,
//                                `data_modified` DATETIME NOT NULL ,
//                                PRIMARY KEY (`id`),
//                                UNIQUE `title_idx` (`title`))
//                                ENGINE = InnoDB;');


    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $schema->dropTable('news');

//        $this->addSql('DROP TABLE `zend-application-skeleton`.`news`');

    }
}
