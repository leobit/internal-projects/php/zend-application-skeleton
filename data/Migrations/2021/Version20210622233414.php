<?php
declare(strict_types=1);

namespace app;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Laminas\Crypt\Password\Bcrypt;

/**
 * Class Version20210622233414
 * @package app
 */
final class Version20210622233414 extends AbstractMigration
{

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return 'A migration which set the `uses` table pre-coded data.';
	}

	/**
	 * @param \Doctrine\DBAL\Schema\Schema $schema
	 */
    public function up(Schema $schema): void
    {
		$bcrypt = new Bcrypt();
		$passwordHash = $bcrypt->create('temp');
		$currentDate = date('Y-m-d H:i:s');
		$users = [
			['email' => 'bv@leobit.com', 'firstName' => 'Bogdan', 'lastName' => 'Vilzhanovskyy']
		];
		foreach ($users as $u) {
			$this->addSql("INSERT INTO user (email, first_name, last_name, password, date_created) 
    	VALUES (
            '".$u['email']."',
            '".$u['firstName']."',
            '".$u['lastName']."',
            '".$passwordHash."',
            '".$currentDate."'
            )");
		}
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
		$this->addSql('DELETE FROM user');
    }
}
