<?php
declare(strict_types=1);

namespace Application\Service\Factory;

use Application\Service\NavbarManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class NavbarManagerFactory
 * @package Application\Service\Factory
 */
class NavbarManagerFactory implements FactoryInterface
{
	/**
	 * @param \Interop\Container\ContainerInterface $container
	 * @param string                                $requestedName
	 * @param array|null                            $options
	 * @return \Application\Service\NavbarManager
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$urlHelperManager = $container->get('ViewHelperManager')->get('url');
		return new NavbarManager($urlHelperManager);
	}
}