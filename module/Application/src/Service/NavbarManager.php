<?php
declare(strict_types=1);

namespace Application\Service;

use Laminas\View\Helper\Url;

/**
 * Class NavbarManager
 * @package Application\Service
 */
class NavbarManager
{
	/**
	 * @var \Laminas\View\Helper\Url
	 */
	protected $urlHelperManager;

	/**
	 * NavbarManager constructor.
	 * @param \Laminas\View\Helper\Url $urlHelperManager
	 */
	public function __construct(Url $urlHelperManager)
	{
		$this->urlHelperManager = $urlHelperManager;
	}

	/**
	 * @return array[]
	 */
	public function getNavigationItems()
	{
		$url = $this->urlHelperManager;
		$items = array(
			[
				'id' => 'home',
				'label' => 'Home',
				'link'  => $url('home')
			],
			[
				'id' => 'users',
				'label' => 'Users Managment',
				'link'  => $url('users')
			],
            [
                'id' => 'news',
                'label' => 'News Managment',
                'link'  => $url('news')
            ],
		);

		return $items;
	}
}