<?php
declare(strict_types=1);

namespace Application\View\Helper;

use Application\Service\NavbarManager;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class Navbar
 * @package Application\View\Helper
 */
class Navbar extends AbstractHelper
{
	/**
	 * @var \Application\Service\NavbarManager
	 */
	protected $navbarManager;

	/**
	 * Navbar constructor.
	 * @param \Application\Service\NavbarManager $navbarManager
	 */
	public function __construct(NavbarManager $navbarManager)
	{
		$this->navbarManager = $navbarManager;
	}

	/**
	 * @return string
	 */
	public function render()
	{
		$items = $this->navbarManager->getNavigationItems();
		$navHtml = '<nav class="navbar navbar-expand-md navbar-dark mb-4" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/img/lion-new.svg" alt="Leobit">Leobit
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">';


		if (!empty($items)) {
			$navHtml .= '<ul class="navbar-nav">';
			foreach ($items as $item){
				$navHtml .= $this->renderItem($item);
			}
			$navHtml .= '</ul>';
		}
		$navHtml .= '
                </div>
            </div>
        </nav>';
		return $navHtml;
	}

	/**
	 * @param $item
	 * @return string
	 */
	protected function renderItem($item)
	{
		$link = isset($item['link']) ? $item['link'] : '#';
		$label = isset($item['label']) ? $item['label'] : '';
		return '<li class="nav-item active">
					<a class="nav-link" href="'.$link.'">
						'.$label.' <span class="sr-only">(current)</span>
					</a>
				</li>';
	}
}