<?php
declare(strict_types=1);

namespace Application\View\Helper\Factory;

use Application\Service\NavbarManager;
use Application\View\Helper\Navbar;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class NavbarFactory
 * @package Application\View\Helper\Factory
 */
class NavbarFactory implements FactoryInterface
{
	/**
	 * @param \Interop\Container\ContainerInterface $container
	 * @param string                                $requestedName
	 * @param array|null                            $options
	 * @return \Application\View\Helper\Navbar
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$navbarManager = $container->get(NavbarManager::class);
		return new Navbar($navbarManager);
	}
}