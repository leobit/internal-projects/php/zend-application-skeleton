<?php
declare(strict_types=1);

namespace Application;

use Laminas\Mvc\MvcEvent;
use Laminas\Session\SessionManager;

class Module
{
    public function getConfig(): array
    {
        /** @var array $config */
        $config = include __DIR__ . '/../config/module.config.php';
        return $config;
    }

	/**
	 * This method is called once the MVC bootstrapping is complete.
	 */
	public function onBootstrap(MvcEvent $event)
	{
		$application = $event->getApplication();
		$serviceManager = $application->getServiceManager();

		// The following line instantiates the SessionManager and automatically
		// makes the SessionManager the 'default' one to avoid passing the
		// session manager as a dependency to other models.
		$sessionManager = $serviceManager->get(SessionManager::class);
	}
}
