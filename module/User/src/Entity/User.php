<?php
declare(strict_types=1);

namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package User\Entity
 * @ORM\Entity(repositoryClass="User\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User 
{
    const STATUS_ACTIVE       = 1; // Active user.
    const STATUS_RETIRED      = 2; // Retired user.

    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /** 
     * @ORM\Column(name="email")  
     */
    protected $email;
    
    /** 
     * @ORM\Column(name="first_name")
     */
    public $firstName;

    /**
     * @ORM\Column(name="last_name")
     */
    public $lastName;

    /** 
     * @ORM\Column(name="password")  
     */
    protected $password;
    
    /**
     * @ORM\Column(name="date_created")  
     */
    protected $dateCreated;
    
    /**
	 * @return mixed
	 */
    public function getId() 
    {
        return $this->id;
    }

    /**
	 * @param $id
	 */
    public function setId($id) 
    {
        $this->id = $id;
    }

    /**
	 * @return mixed
	 */
    public function getEmail() 
    {
        return $this->email;
    }

    /**
	 * @param $email
	 */
    public function setEmail($email) 
    {
        $this->email = $email;
    }
    
    /**
	 * @return mixed
	 */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
	 * @return mixed
	 */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
	 * @param $firstName
	 */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
	 * @param $lastName
	 */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

	/**
	 * @return mixed
	 */
    public function getPassword() 
    {
       return $this->password; 
    }

	/**
	 * @param $password
	 */
    public function setPassword($password) 
    {
        $this->password = $password;
    }

	/**
	 * @return mixed
	 */
    public function getDateCreated() 
    {
        return $this->dateCreated;
    }

	/**
	 * @param $dateCreated
	 */
    public function setDateCreated($dateCreated) 
    {
        $this->dateCreated = $dateCreated;
    }
}