<?php

declare(strict_types=1);

namespace User\Validator;

use Laminas\Validator\AbstractValidator;
use User\Entity\User;

class UserExistsValidator extends AbstractValidator
{
    /**
     * Available validator options.
     * @var array
     */
    protected $options = array(
        'entityManager' => null,
        'user' => null
    );

    const NOT_SCALAR = 'notScalar';
    const USER_EXISTS = 'userExists';

    /**
     * Validation failure messages.
     * @var array
     */
    protected $messageTemplates = array(
        self::NOT_SCALAR => "The email must be a scalar value",
        self::USER_EXISTS => "Another user with such an email already exists"
    );

    /**
     * @param null $options
     */
    public function __construct($options = null)
    {
        if (is_array($options)) {
            if (isset($options['entityManager'])) {
                $this->options['entityManager'] = $options['entityManager'];
            }
            if (isset($options['user'])) {
                $this->options['user'] = $options['user'];
            }
        }

        parent::__construct($options);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function isValid($value)
    {
        if (!is_scalar($value)) {
            $this->error(self::NOT_SCALAR);
            return false;
        }

        $entityManager = $this->options['entityManager'];

        $user = $entityManager->getRepository(User::class)->findOneByEmail($value);

        if ($this->options['user'] == null) {
            $isValid = ($user == null);
        } else {
            if ($this->options['user']->getEmail() != $value && $user != null) {
                $isValid = false;
            } else {
                $isValid = true;
            }
        }

        if (!$isValid) {
            $this->error(self::USER_EXISTS);
        }

        return $isValid;
    }
}