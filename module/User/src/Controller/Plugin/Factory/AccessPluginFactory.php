<?php
declare(strict_types=1);

namespace User\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use User\Controller\Plugin\AccessPlugin;


/**
 * Class AccessPluginFactory
 * @package User\Controller\Plugin\Factory
 */
class AccessPluginFactory implements FactoryInterface
{
	/**
	 * @param \Interop\Container\ContainerInterface $container
	 * @param string                                $requestedName
	 * @param array|null                            $options
	 * @return \User\Controller\Plugin\AccessPlugin
	 */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AccessPlugin();
    }
}