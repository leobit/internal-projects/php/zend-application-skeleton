<?php
declare(strict_types=1);

namespace User\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Laminas\Authentication\AuthenticationService;
use User\Controller\Plugin\CurrentUserPlugin;

/**
 * Class CurrentUserPluginFactory
 * @package User\Controller\Plugin\Factory
 */
class CurrentUserPluginFactory
{
	/**
	 * @param \Interop\Container\ContainerInterface $container
	 * @return \User\Controller\Plugin\CurrentUserPlugin
	 */
    public function __invoke(ContainerInterface $container)
    {        
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $authService = $container->get(AuthenticationService::class);
        
        return new CurrentUserPlugin($entityManager, $authService);
    }
}