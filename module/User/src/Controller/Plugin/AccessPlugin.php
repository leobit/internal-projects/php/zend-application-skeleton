<?php
declare(strict_types=1);

namespace User\Controller\Plugin;

use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class AccessPlugin
 * @package User\Controller\Plugin
 */
class AccessPlugin extends AbstractPlugin
{

	/**
	 * @param       $permission
	 * @param array $params
	 * @return mixed
	 */
    public function __invoke($permission, $params = [])
    {
        return true;
    }
}