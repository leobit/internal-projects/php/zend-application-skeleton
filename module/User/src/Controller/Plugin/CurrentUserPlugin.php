<?php
declare(strict_types=1);

namespace User\Controller\Plugin;

use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use User\Entity\User;

/**
 * Class CurrentUserPlugin
 * @package User\Controller\Plugin
 */
class CurrentUserPlugin extends AbstractPlugin
{
    private $entityManager;
    private $authService;
    private $user = null;

	/**
	 * CurrentUserPlugin constructor.
	 * @param $entityManager
	 * @param $authService
	 */
    public function __construct($entityManager, $authService) 
    {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
    }

	/**
	 * @param bool $useCachedUser
	 * @return \User\Entity\User\Entity\User|null
	 * @throws \Exception
	 */
    public function __invoke($useCachedUser = true)
    {
        if ($useCachedUser && $this->user !== null)
            return $this->user;

        if ($this->authService->hasIdentity()) {
            $this->user = $this->entityManager->getRepository(User::class)
                    ->findOneByEmail($this->authService->getIdentity());
            if (is_null($this->user)) {
                throw new \Exception('User not found');
            }
            return $this->user;
        }
        
        return null;
    }
}