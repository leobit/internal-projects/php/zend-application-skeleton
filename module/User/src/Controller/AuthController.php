<?php
declare(strict_types=1);

namespace User\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use User\Form\LoginForm;
use Laminas\Authentication\Result;
use Laminas\Uri\Uri;


/**
 * Class AuthController
 * @package User\Controller
 */
class AuthController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager.
     * @var User\Service\UserManager
     */
    private $userManager;

    /**
     * Auth manager.
     * @var User\Service\AuthManager
     */
    private $authManager;

	/**
	 * AuthController constructor.
	 * @param $entityManager
	 */
    public function __construct($entityManager, $userManager, $authManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->authManager = $authManager;
    }

	/**
	 * @return \Laminas\Http\Response|\Laminas\View\Model\ViewModel
	 * @throws \Exception
	 */
    public function loginAction()
    {
        $form = new LoginForm();
        $redirectUrl = (string)$this->params()->fromQuery('redirectUrl', '');
        $form->get('redirect_url')->setValue($redirectUrl);
        $isAuthError = false;

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if($form->isValid()) {
                $data = $form->getData();
                $result = $this->authManager->login($data['email'], $data['password'], $data['remember_me']);
                if ($result->getCode() == Result::SUCCESS) {
                    $redirectUrl = $this->params()->fromPost('redirect_url', '');
                    if (!empty($redirectUrl)) {
                        $uri = new Uri($redirectUrl);
                        if (!$uri->isValid() || $uri->getHost()!=null)
                            throw new \Exception('Incorrect redirect URL: ' . $redirectUrl);
                    }
                    if(empty($redirectUrl)) {
                        return $this->redirect()->toRoute('home');
                    } else {
                        $this->redirect()->toUrl($redirectUrl);
                    }
                } else {
                    $isAuthError = true;
                }
            } else {
                $isAuthError = true;
            }
        }
        return new ViewModel([
            'form' => $form,
            'redirectUrl' => $redirectUrl,
            'isError' => $isAuthError
        ]);
    }

	/**
	 * @return \Laminas\Http\Response
	 */
    public function logoutAction()
    {
		return new ViewModel();
    }

	/**
	 * @return \Laminas\View\Model\ViewModel
	 */
    public function notAllowedAction()
    {
        $this->getResponse()->setStatusCode(403);
        return new ViewModel();
    }
}
