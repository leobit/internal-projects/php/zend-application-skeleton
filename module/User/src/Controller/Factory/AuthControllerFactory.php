<?php
declare(strict_types=1);

namespace User\Controller\Factory;

use Interop\Container\ContainerInterface;
use User\Controller\AuthController;
use Laminas\ServiceManager\Factory\FactoryInterface;
use User\Service\AuthManager;
use User\Service\UserManager;

/**
 * Class AuthControllerFactory
 * @package User\Controller\Factory
 */
class AuthControllerFactory implements FactoryInterface
{
	/**
	 * @param \Interop\Container\ContainerInterface $container
	 * @param string                                $requestedName
	 * @param array|null                            $options
	 * @return \User\Controller\AuthController
	 */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $authManager = $container->get(AuthManager::class);

        return new AuthController($entityManager, $userManager, $authManager);
    }
}
