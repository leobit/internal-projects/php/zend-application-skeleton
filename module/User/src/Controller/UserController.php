<?php

declare(strict_types=1);

namespace User\Controller;

use Exception;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use User\Entity\User;
use User\Form\PasswordChangeForm;
use User\Form\UserForm;

class UserController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager.
     * @var User\Service\UserManager
     */
    private $userManager;

    /**
     * Auth manager.
     * @var User\Service\AuthManager
     */
    private $authManager;

    /**
     * @param $entityManager
     * @param $userManager
     * @param $authManager
     */
    public function __construct($entityManager, $userManager, $authManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->authManager = $authManager;
    }

    /**
     * @return ViewModel|void
     */
    public function indexAction()
    {
        $users = $this->entityManager->getRepository(User::class)->findBy([], ['id' => 'ASC']);

        return new ViewModel([
                                 'users' => $users
                             ]);
    }

    /**
     * @return Response|ViewModel
     */
    public function addAction()
    {
        $form = new UserForm('create', $this->entityManager);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                $user = $this->userManager->addUser($data);
                return $this->redirect()->toRoute(
                    'users',
                    ['action' => 'view', 'id' => $user->getId()]
                );
            }
        }

        return new ViewModel([
                                 'form' => $form
                             ]);
    }

    /**
     * @return ViewModel|void
     */
    public function viewAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $user = $this->entityManager->getRepository(User::class)->find($id);

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        return new ViewModel([
                                 'user' => $user
                             ]);
    }

    /**
     * @return Response|ViewModel|void
     */
    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $user = $this->entityManager->getRepository(User::class)->find($id);

        if (is_null($user)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $form = new UserForm('update', $this->entityManager, $user);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->userManager->updateUser($user, $data);
                return $this->redirect()->toRoute('users', ['action' => 'view', 'id' => $user->getId()]);
            }
        } else {
            $form->setData(array(
                               'email' => $user->getEmail(),
                               'last_name' => $user->getLastName(),
                               'first_name' => $user->getFirstName(),
                           ));
        }
        return new ViewModel(array(
                                 'user' => $user,
                                 'form' => $form
                             ));
    }

    /**
     * @return Response|void
     */
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $user = $this->entityManager->getRepository(User::class)->find($id);

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->userManager->deleteUser($user);

        $this->flashMessenger()->addSuccessMessage('Deleted the user.');
        return $this->redirect()->toRoute('users', ['action' => 'index']);
    }

    /**
     * @return Response|ViewModel|void
     */
    public function changePasswordAction()
    {
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('email' => $this->authManager->getAuthService()->getIdentity()));

        if (is_null($user)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $form = new PasswordChangeForm('change');
        $form->remove('old_password');
        $form->getInputFilter()->remove('old_password');

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                $data = $form->getData();
                if (!$this->userManager->changePassword($user, $data)) {
                    $this->flashMessenger()->addErrorMessage(
                        'Sorry, the old password is incorrect. Could not set the new password.'
                    );
                } else {
                    $this->flashMessenger()->addSuccessMessage('Changed the password successfully.');
                }

                return $this->redirect()->toRoute('users', ['action' => 'view', 'id' => $user->getId()]);
            }
        }

        return new ViewModel([
                                 'user' => $user,
                                 'form' => $form
                             ]);
    }

    /**
     * @return Response|ViewModel
     */
    public function resetPasswordAction()
    {
        $form = new PasswordResetForm();
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $form->setData($data);

            if ($form->isValid()) {
                $user = $this->entityManager->getRepository(User::class)->findOneByEmail($data['email']);
                if (!is_null($user)) {
                    $this->userManager->generatePasswordResetToken($user);

                    return $this->redirect()->toRoute('users', ['action' => 'message', 'id' => 'sent']);
                } else {
                    return $this->redirect()->toRoute('users', ['action' => 'message', 'id' => 'invalid-email']);
                }
            }
        }

        return new ViewModel([
                                 'form' => $form
                             ]);
    }

    /**
     * @return ViewModel
     * @throws Exception
     */
    public function messageAction()
    {
        $id = (string)$this->params()->fromRoute('id');
        if ($id != 'invalid-email' && $id != 'sent' && $id != 'set' && $id != 'failed') {
            throw new Exception('Invalid message ID specified');
        }

        return new ViewModel([
                                 'id' => $id
                             ]);
    }

    /**
     * @return Response|ViewModel
     * @throws Exception
     */
    public function setPasswordAction()
    {
        $token = $this->params()->fromQuery('token', null);

        if ($token != null && (!is_string($token) || strlen($token) != 32)) {
            throw new Exception('Invalid token type or length');
        }

        if ($token === null ||
            !$this->userManager->validatePasswordResetToken($token)) {
            return $this->redirect()->toRoute('users', ['action' => 'message', 'id' => 'failed']);
        }

        $form = new PasswordChangeForm('reset');
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                if ($this->userManager->setNewPasswordByToken($token, $data['new_password'])) {
                    return $this->redirect()->toRoute('users', ['action' => 'message', 'id' => 'set']);
                } else {
                    return $this->redirect()->toRoute('users', ['action' => 'message', 'id' => 'failed']);
                }
            }
        }

        return new ViewModel([
                                 'form' => $form
                             ]);
    }
}