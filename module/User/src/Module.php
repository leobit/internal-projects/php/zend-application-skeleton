<?php
declare(strict_types=1);

namespace User;

use Laminas\Mvc\MvcEvent;
use Laminas\Mvc\Controller\AbstractActionController;
use User\Controller\AuthController;
use User\Service\AuthManager;

/**
 * Class Module
 * @package User
 */
class Module
{
	/**
	 * @return mixed
	 */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

	/**
	 * @param \Laminas\Mvc\MvcEvent $event
	 */
    public function onBootstrap(MvcEvent $event)
    {
        $eventManager = $event->getApplication()->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();
        $sharedEventManager->attach(
            AbstractActionController::class,
            MvcEvent::EVENT_DISPATCH,
            [$this, 'onDispatch'],
            100
        );
    }

	/**
	 * @param \Laminas\Mvc\MvcEvent $event
	 * @return mixed
	 */
    public function onDispatch(MvcEvent $event)
    {
        $controller = $event->getTarget();
        $controllerName = $event->getRouteMatch()->getParam('controller', null);
        $actionName = $event->getRouteMatch()->getParam('action', null);
        $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));
        $authManager = $event->getApplication()->getServiceManager()->get(AuthManager::class);

        if ($controllerName != AuthController::class) {
            $result = $authManager->filterAccess($controllerName, $actionName);
            if ($result == AuthManager::AUTH_REQUIRED) {
                $uri = $event->getApplication()->getRequest()->getUri();
                $uri->setScheme(null)
                    ->setHost(null)
                    ->setPort(null)
                    ->setUserInfo(null);
                $redirectUrl = $uri->toString();

                return $controller->redirect()->toRoute('login', [],
                        ['query'=>['redirectUrl'=>$redirectUrl]]);
            } else if ($result == AuthManager::ACCESS_DENIED) {
                return $controller->redirect()->toRoute('not-allowed');
            }
        }
    }
}
