<?php
declare(strict_types=1);

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use User\Service\AuthAdapter;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class AuthAdapterFactory
 * @package User\Service\Factory
 */
class AuthAdapterFactory implements FactoryInterface
{
	/**
	 * @param \Interop\Container\ContainerInterface $container
	 * @param                                       $requestedName
	 * @param array|null                            $options
	 * @return \User\Service\AuthAdapter
	 */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        return new AuthAdapter($entityManager);
    }
}