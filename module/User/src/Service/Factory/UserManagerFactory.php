<?php
declare(strict_types=1);

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use User\Service\UserManager;

/**
 * Class UserManagerFactory
 * @package User\Service\Factory
 */
class UserManagerFactory
{
    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param                                       $requestedName
     * @param array|null                            $options
     * @return \User\Service\UserManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {        
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        
        return new UserManager($entityManager);
    }
}
