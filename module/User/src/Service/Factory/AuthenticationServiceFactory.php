<?php
declare(strict_types=1);

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use Laminas\Authentication\AuthenticationService;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\Session\SessionManager;
use Laminas\Authentication\Storage\Session as SessionStorage;
use User\Service\AuthAdapter;

/**
 * Class AuthenticationServiceFactory
 * @package User\Service\Factory
 */
class AuthenticationServiceFactory implements FactoryInterface
{
	/**
	 * @param \Interop\Container\ContainerInterface $container
	 * @param string                                $requestedName
	 * @param array|null                            $options
	 * @return \Laminas\Authentication\AuthenticationService
	 */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $authStorage = new SessionStorage('Zend_Auth', 'session', $container->get(SessionManager::class));
        $authAdapter = $container->get(AuthAdapter::class);

        return new AuthenticationService($authStorage, $authAdapter);
    }
}