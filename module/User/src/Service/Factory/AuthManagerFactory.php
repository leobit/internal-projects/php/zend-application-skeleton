<?php
declare(strict_types=1);

namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\Authentication\AuthenticationService;
use Laminas\Session\SessionManager;
use User\Service\AuthManager;
use User\Service\RbacManager;

/**
 * Class AuthManagerFactory
 * @package User\Service\Factory
 */
class AuthManagerFactory implements FactoryInterface
{
    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param string                                $requestedName
     * @param array|null                            $options
     * @return \User\Service\AuthManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $authenticationService = $container->get(AuthenticationService::class);
        $sessionManager = $container->get(SessionManager::class);
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $config = $container->get('Config');
        if (isset($config['access_filter'])) {
            $config = $config['access_filter'];
        } else {
            $config = [];
        }

        return new AuthManager($entityManager, $authenticationService, $sessionManager, $config);
    }
}
