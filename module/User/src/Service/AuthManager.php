<?php
declare(strict_types=1);

namespace User\Service;

use Laminas\Authentication\Result;

/**
 * Class AuthManager
 * @package User\Service
 */
class AuthManager
{
	const ACCESS_GRANTED = 1;
	const AUTH_REQUIRED  = 2;
	const ACCESS_DENIED  = 3;

    private $authService;
    private $sessionManager;
    private $config;
    
    /**
     * Constructs the service.
     */
    public function __construct($entityManager, $authService, $sessionManager, $config)
    {
        $this->authService = $authService;
        $this->sessionManager = $sessionManager;
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getAuthService()
    {
        return $this->authService;
    }

    /**
     * @return mixed
     */
    public function getSessionManager()
    {
        return $this->sessionManager;
    }

    /**
     * @param $email
     * @param $password
     * @param $rememberMe
     * @return mixed
     * @throws \Exception
     */
    public function login($email, $password, $rememberMe)
    {
        if ($this->authService->getIdentity() != null) {
            throw new \Exception('Already logged in');
        }

        $authAdapter = $this->authService->getAdapter();
        $authAdapter->setEmail($email);
        $authAdapter->setPassword($password);
        $result = $this->authService->authenticate();

        if ($result->getCode() == Result::SUCCESS && $rememberMe) {
            $this->sessionManager->rememberMe(60*60*24*30);
        }
        
        return $result;
    }

    /**
     * @throws \Exception
     */
    public function logout()
    {
        if ($this->authService->getIdentity() == null) {
            throw new \Exception('The user is not logged in');
        }

        $this->authService->clearIdentity();               
    }

    /**
     * @param $controllerName
     * @param $actionName
     * @return int
     * @throws \Exception
     */
    public function filterAccess($controllerName, $actionName)
    {
        $mode = isset($this->config['options']['mode'])?$this->config['options']['mode']:'restrictive';
        if ($mode!='restrictive' && $mode!='permissive') {
            throw new \Exception('Invalid access filter mode (expected either restrictive or permissive mode');
        }
        
        if (isset($this->config['controllers'][$controllerName])) {
            $items = $this->config['controllers'][$controllerName];
            foreach ($items as $item) {
                $actionList = $item['actions'];
                $allow = $item['allow'];
                if (is_array($actionList) && in_array($actionName, $actionList) ||
                    $actionList=='*') {
                    if ($allow=='*') {
                        return self::ACCESS_GRANTED;
                    } else if (!$this->authService->hasIdentity()) {
                        return self::AUTH_REQUIRED;                        
                    }
                        
                    if ($allow=='@') {
                        return self::ACCESS_GRANTED;                         
                    } else if (substr($allow, 0, 1)=='@') {
                        $identity = substr($allow, 1);
                        if ($this->authService->getIdentity()==$identity) {
                            return self::ACCESS_GRANTED;
                        } else {
                            return self::ACCESS_DENIED;
                        }
                    } else if (substr($allow, 0, 1)=='+') {
                        $permission = substr($allow, 1);
                        if ($this->rbacManager->isGranted(null, $permission)) {
                            return self::ACCESS_GRANTED;
                        } else {
                            return self::ACCESS_DENIED;
                        }
                    } else {
                        throw new \Exception('Unexpected value for "allow" - expected ' .
                                'either "?", "@", "@identity" or "+permission"');
                    }
                }
            }            
        }

        if ($mode=='restrictive') {
            if(!$this->authService->hasIdentity()) {
                return self::AUTH_REQUIRED;
            } else {
                return self::ACCESS_DENIED;
            }
        }

        return self::ACCESS_GRANTED;
    }
}