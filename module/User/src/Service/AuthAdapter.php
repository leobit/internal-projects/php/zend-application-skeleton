<?php
declare(strict_types=1);

namespace User\Service;

use Laminas\Authentication\Adapter\AdapterInterface;
use Laminas\Authentication\Result;
use Laminas\Crypt\Password\Bcrypt;
use User\Entity\User;

/**
 * Class AuthAdapter
 * @package User\Service
 */
class AuthAdapter implements AdapterInterface
{

    private $email;
    private $password;

    private $entityManager;

	/**
	 * AuthAdapter constructor.
	 * @param $entityManager
	 */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

	/**
	 * @param $email
	 */
    public function setEmail($email) 
    {
        $this->email = $email;        
    }

	/**
	 * @param $password
	 */
    public function setPassword($password) 
    {
        $this->password = $password;
    }

	/**
	 * @return \Laminas\Authentication\Result
	 */
    public function authenticate()
    {
        $user = $this->entityManager->getRepository(User::class)
                ->findOneByEmail($this->email);
        if (is_null($user)) {
            return new Result(
                Result::FAILURE_IDENTITY_NOT_FOUND, 
                null, 
                ['Invalid credentials.']);        
        }

        $bcrypt = new Bcrypt();
        if ($bcrypt->verify($this->password, $user->getPassword())) {
            return new Result(
                    Result::SUCCESS, 
                    $this->email, 
                    ['Authenticated successfully.']);        
        }

        return new Result(
                Result::FAILURE_CREDENTIAL_INVALID, 
                null, 
                ['Invalid credentials.']);        
    }
}