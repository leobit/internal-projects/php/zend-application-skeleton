<?php

declare(strict_types=1);

namespace User\Service;

use Exception;
use User\Entity\User;
use Laminas\Crypt\Password\Bcrypt;

/**
 * Class UserManager
 * @package User\Service
 */
class UserManager
{
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * UserManager constructor.
     * @param $entityManager
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $email
     * @return bool
     */
    public function checkUserExists($email)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneByEmail($email);
        return $user !== null;
    }

    /**
     * @param $user
     * @param $password
     * @return bool
     */
    public function validatePassword($user, $password)
    {
        $bcrypt = new Bcrypt();
        $passwordHash = $user->getPassword();
        if ($bcrypt->verify($password, $passwordHash)) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return User
     * @throws Exception
     */
    public function addUser($data)
    {
        if ($this->checkUserExists($data['email'])) {
            throw new Exception("User with email address " . $data['$email'] . " already exists");
        }

        $user = new User();
        $user->setEmail($data['email']);
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['last_name']);
        $user->setDateCreated(date('Y-m-d H:i:s'));

        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create($data['password']);
        $user->setPassword($passwordHash);

        $this->entityManager->persist($user);

        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function updateUser(User $user, $data)
    {
        if ($user->getEmail() != $data['email'] && $this->checkUserExists($data['email'])) {
            throw new Exception("Another user with email address " . $data['email'] . " already exists");
        }
        $user->setEmail($data['email']);
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['last_name']);
        $this->entityManager->flush();
        return true;
    }

    /**
     * @param User $user
     */
    public function deleteUser(User $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     * @param $data
     * @return bool
     */
    public function changePassword(User $user, $data)
    {
        $newPassword = $data['new_password'];
        if (strlen($newPassword) < 6 || strlen($newPassword) > 64) {
            return false;
        }
        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create($newPassword);
        $user->setPassword($passwordHash);
        $this->entityManager->flush();

        return true;
    }
}

