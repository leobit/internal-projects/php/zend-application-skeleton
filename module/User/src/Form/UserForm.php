<?php

declare(strict_types=1);

namespace User\Form;

use Laminas\Form\Form;
use Laminas\InputFilter\InputFilter;
use Laminas\Validator\Hostname;
use User\Validator\UserExistsValidator;

class UserForm extends Form
{
    /**
     * Scenario ('create' or 'update').
     * @var string
     */
    private $scenario;

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * Current user.
     * @var User\Entity\User
     */
    private $user = null;

    /**
     * @param string $scenario
     * @param null $entityManager
     * @param null $user
     */
    public function __construct($scenario = 'create', $entityManager = null, $user = null)
    {
        parent::__construct('user-form');
        $this->setAttribute('method', 'post');
        $this->scenario = $scenario;
        $this->entityManager = $entityManager;
        $this->user = $user;

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
                       'type' => 'text',
                       'name' => 'email',
                       'options' => [
                           'label' => 'E-mail',
                       ],
                   ]);

        $this->add([
                       'type' => 'text',
                       'name' => 'first_name',
                       'options' => [
                           'label' => 'First Name',
                       ],
                   ]);

        $this->add([
                       'type' => 'text',
                       'name' => 'last_name',
                       'options' => [
                           'label' => 'Last Name',
                       ],
                   ]);

        if ($this->scenario == 'create') {
            $this->add([
                           'type' => 'password',
                           'name' => 'password',
                           'options' => [
                               'label' => 'Password',
                           ],
                       ]);

            $this->add([
                           'type' => 'password',
                           'name' => 'confirm_password',
                           'options' => [
                               'label' => 'Confirm password',
                           ],
                       ]);
        }

        $this->add([
                       'type' => 'submit',
                       'name' => 'submit',
                       'attributes' => [
                           'value' => 'Create'
                       ],
                   ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
                              'name' => 'email',
                              'required' => true,
                              'filters' => [
                                  ['name' => 'StringTrim'],
                              ],
                              'validators' => [
                                  [
                                      'name' => 'StringLength',
                                      'options' => [
                                          'min' => 1,
                                          'max' => 128
                                      ],
                                  ],
                                  [
                                      'name' => 'EmailAddress',
                                      'options' => [
                                          'allow' => Hostname::ALLOW_DNS,
                                          'useMxCheck' => false,
                                      ],
                                  ],
                                  [
                                      'name' => UserExistsValidator::class,
                                      'options' => [
                                          'entityManager' => $this->entityManager,
                                          'user' => $this->user
                                      ],
                                  ],
                              ],
                          ]);

        $inputFilter->add([
                              'name' => 'first_name',
                              'required' => true,
                              'filters' => [
                                  ['name' => 'StringTrim'],
                              ],
                              'validators' => [
                                  [
                                      'name' => 'StringLength',
                                      'options' => [
                                          'min' => 1,
                                          'max' => 512
                                      ],
                                  ],
                              ],
                          ]);

        $inputFilter->add([
                              'name' => 'last_name',
                              'required' => true,
                              'filters' => [
                                  ['name' => 'StringTrim'],
                              ],
                              'validators' => [
                                  [
                                      'name' => 'StringLength',
                                      'options' => [
                                          'min' => 1,
                                          'max' => 512
                                      ],
                                  ],
                              ],
                          ]);

        if ($this->scenario == 'create') {
            $inputFilter->add([
                                  'name' => 'password',
                                  'required' => true,
                                  'filters' => [
                                  ],
                                  'validators' => [
                                      [
                                          'name' => 'StringLength',
                                          'options' => [
                                              'min' => 6,
                                              'max' => 64
                                          ],
                                      ],
                                  ],
                              ]);

            $inputFilter->add([
                                  'name' => 'confirm_password',
                                  'required' => true,
                                  'filters' => [
                                  ],
                                  'validators' => [
                                      [
                                          'name' => 'Identical',
                                          'options' => [
                                              'token' => 'password',
                                          ],
                                      ],
                                  ],
                              ]);
        }
    }
}