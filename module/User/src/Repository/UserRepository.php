<?php
declare(strict_types=1);

namespace User\Repository;
use Doctrine\ORM\EntityRepository;
use User\Entity\User;

/**
 * Class UserRepository
 * @package User\Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @param $email
     * @return mixed
     */
    public function findUserByEmail($email)
    {
        return $this->getUserQueryByEmail($email)->getOneOrNullResult();
    }

    /**
     * @param $email
     * @return \Doctrine\ORM\Query
     */
    private function getUserQueryByEmail($email)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
            ->from(User::class, 'a')
            ->where('a.email = ?1')
            ->setParameter('1', $email);

        return $queryBuilder->getQuery();
    }
}