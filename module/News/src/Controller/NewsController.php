<?php

namespace News\Controller;

use Exception;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use News\Entity\News;
use News\Form\NewsForm;

class NewsController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Auth manager.
     * @var News\Service\NewsManager
     */
    private $newsManager;

    /**
     * @param $entityManager
     * @param $newsManager
     */
    public function __construct($entityManager, $newsManager)
    {
        $this->entityManager = $entityManager;
        $this->newsManager = $newsManager;
    }

    /**
     * @return ViewModel|void
     */
    public function indexAction()
    {
        $news = $this->entityManager->getRepository(News::class)->findBy([], ['id' => 'ASC']);

        return new ViewModel([
            'news' => $news
        ]);
    }

    /**
     * @return Response|ViewModel
     */
    public function addAction()
    {
        $form = new NewsForm($this->entityManager);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                $news = $this->newsManager->addNews($data);
//                return $this->redirect()->toRoute('news', ['action' => 'index']);
                return $this->redirect()->toRoute('news',['action' => 'view', 'id' => $news->getId()]);
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }


    /**
     * @return Response|ViewModel|void
     */
    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $news = $this->entityManager->getRepository(News::class)->find($id);

        if ($news == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $form = new NewsForm($this->entityManager, $news);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->newsManager->updateNews($news, $data);
//                return $this->redirect()->toRoute('news', ['action' => 'index']);
                return $this->redirect()->toRoute('news',['action' => 'view', 'id' => $news->getId()]);

            }
        } else {
            $form->setData(array(
                'title' => $news->getTitle(),
                'description' => $news->getDescription(),
                'authors' => $news->getAuthors(),
                'content' => $news->getContent(),
            ));
        }

        return new ViewModel(array(
            'news' => $news,
            'form' => $form
        ));
    }


    /**
     * @return Response|void
     */
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $news = $this->entityManager->getRepository(News::class)->find($id);

        if ($news == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->newsManager->deleteNews($news);

        $this->flashMessenger()->addSuccessMessage('Deleted the news.');
        return $this->redirect()->toRoute('news', ['action' => 'index']);

    }

    /**
     * @return ViewModel|void
     */
    public function viewAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $news = $this->entityManager->getRepository(News::class)->find($id);

        if ($news == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        return new ViewModel([
            'news' => $news
        ]);
    }

}