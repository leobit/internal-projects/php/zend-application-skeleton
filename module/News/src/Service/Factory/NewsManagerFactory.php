<?php

namespace News\Service\Factory;

use Interop\Container\ContainerInterface;
use News\Service\NewsManager;

/**
 * Class NewsManagerFactory
 * @package News\Service\Factory
 */

class NewsManagerFactory
{
    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param                                       $requestedName
     * @param array|null                            $options
     * @return \News\Service\NewsManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new NewsManager($entityManager);
    }
}