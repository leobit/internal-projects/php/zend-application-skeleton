<?php

namespace News\Service;

use Exception;
use News\Entity\News;


/**
 * Class NewsManager
 * @package News\Service
 */

class NewsManager
{
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * NewsManager constructor.
     * @param $entityManager
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $title
     * @return bool
     */
    public function checkNewsExists($title)
    {
        $news = $this->entityManager->getRepository(News::class)->findOneByTitle($title);
        return $news !== null;
    }


    /**
     * @param $data
     * @return News
     * @throws Exception
     */
    public function addNews($data)
    {
        if ($this->checkNewsExists($data['title'])) {
            throw new Exception("The news with title: " . $data['$title'] . " already exists");
        }
        $news = new News();
        $news->setTitle($data['title']);
        $news->setDescription($data['description']);
        $news->setAuthors($data['authors']);
        $news->setContent($data['content']);

        $news->setDateCreated(date('Y-m-d H:i:s'));
        $news->setDateModified(date('Y-m-d H:i:s'));

        $this->entityManager->persist($news);

        $this->entityManager->flush();

        return $news;
    }

    /**
     * @param News $news
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function updateNews(News $news, $data)
    {
        if ($news->getTitle() != $data['title'] && $this->checkNewsExists($data['title'])) {
            throw new Exception("Another news with  the same title: " . $data['title'] . " already exists");
        }

        $news->setTitle($data['title']);
        $news->setDescription($data['description']);
        $news->setAuthors($data['authors']);
        $news->setContent($data['content']);
        $news->setDateModified(date('Y-m-d H:i:s'));

        $this->entityManager->persist($news);

        $this->entityManager->flush();
        return true;
    }

    /**
     * @param News $news
     */
    public function deleteNews(News $news)
    {
        $this->entityManager->remove($news);
        $this->entityManager->flush();
    }

}