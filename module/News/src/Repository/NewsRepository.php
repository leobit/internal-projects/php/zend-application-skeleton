<?php

namespace News\Repository;

use Doctrine\ORM\EntityRepository;
use News\Entity\News;

class NewsRepository extends EntityRepository
{
    public function findNewsByTitle($title)
    {
        return $this->getNewsQueryByTitle($title)->getOneOrNullResult();
    }

    /**
     * @param $title
     * @return \Doctrine\ORM\Query
     */
    private function getNewsQueryByTitle($title)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
            ->from(News::class, 'a')
            ->where('a.title = ?1')
            ->setParameter('1', $title);

        return $queryBuilder->getQuery();
    }
}