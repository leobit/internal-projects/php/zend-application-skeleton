<?php

namespace News\Form;

use Laminas\Form\Form;

class NewsForm  extends Form
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * Current news.
     * @var News\Entity\News
     */
    private $news = null;

    /**
     * @param null $entityManager
     * @param null $news
     */
    public function __construct($entityManager = null, $news = null)
    {
        parent::__construct('news-form');
        $this->setAttribute('method', 'post');
        $this->entityManager = $entityManager;
        $this->news = $news;

        $this->addElements();
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'title',
            'options' => [
                'label' => 'Title',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'description',
            'options' => [
                'label' => 'Description',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'authors',
            'options' => [
                'label' => 'Authors',
            ],
        ]);

        $this->add([
            'type' => 'textarea',
            'name' => 'content',
            'options' => [
                'label' => 'Content',
            ],
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create'
            ],
        ]);
    }
}